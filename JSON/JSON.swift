
import Foundation

public typealias JSONDictionary = [String: AnyObject]

public struct JSON: JSONObject {
    private let json: JSONDictionary
    
    public init(json: JSONDictionary) {
        self.json = json
    }
    
    public init(json: JSON) throws {
        self.json = json.json
    }
    
    public static func decode(data: Data) throws -> JSON {
        do {
            
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionary else {
                throw JSONParsingError.notJsonConvertible
            }
            
            return JSON(json: json)
        } catch {
            throw JSONParsingError.notJsonConvertible
        }
    }
    
    // MARK: Pirimitive Types
    
    public func get<T: _JSONPrimitive>(_ key: String) throws -> T {
        guard let obj = json[key] else {
            throw JSONParsingError.notFound(key: key)
        }
        
        guard let value = obj as? T else {
            throw JSONParsingError.wrongType(key: key, type: obj.dynamicType,
                                             expectedType: T.self)
        }
        
        return value
    }
    
    public func get<T: _JSONPrimitive>(_ key: String) -> T? {
        return json[key] as? T
    }
    
    public func get<T: _JSONPrimitive>(_ key: String) throws -> [T] {
        guard let obj = json[key] else {
            throw JSONParsingError.notFound(key: key)
        }
        
        guard let value = obj as? [T] else {
            throw JSONParsingError.wrongType(key: key, type: obj.dynamicType,
                                             expectedType: [T].self)
        }
        
        return value
    }

    public func get<T: _JSONPrimitive>(_ key: String) -> [T]? {
        return json[key] as? [T]
    }
    
    // MARK: Objects
    
    public func get<T: JSONObject>(_ key: String) throws -> T {
        guard let obj = json[key] else {
            throw JSONParsingError.notFound(key: key)
        }
        
        guard let value = obj as? [String: AnyObject] else {
            throw JSONParsingError.wrongType(key: key, type: obj.dynamicType,
                                             expectedType: JSONDictionary.self)
        }
        
        return try T(json: JSON(json: value))
    }
    
    public func get<T: JSONObject>(_ key: String) throws -> T? {
        guard let obj = json[key] else {
            return nil
        }
        
        guard let value = obj as? [String: AnyObject] else {
            if obj is NSNull {
                return nil
            } else {
                throw JSONParsingError.wrongType(key: key, type: obj.dynamicType,
                                                 expectedType: JSONDictionary.self)
            }
        }
        
        return try T(json: JSON(json: value))
    }
    
    public func get<T: JSONObject>(_ key: String) throws -> [T] {
        guard let obj = json[key] else {
            throw JSONParsingError.notFound(key: key)
        }
        
        guard let val = obj as? [[String: AnyObject]] else {
            throw JSONParsingError.wrongType(key: key, type: obj.dynamicType,
                                             expectedType: [JSONDictionary].self)
        }
        
        return try val.map { try T(json: JSON(json: $0)) }
    }

    public func get<T: JSONObject>(_ key: String) throws -> [T]? {
        guard let obj = json[key] else {
            return nil
        }
        
        guard let val = obj as? [[String: AnyObject]] else {
            throw JSONParsingError.wrongType(key: key, type: obj.dynamicType,
                                             expectedType: [JSONDictionary].self)
        }
        
        return try val.map { try T(json: JSON(json: $0)) }
    }
    
    public func get<T: _JSONPrimitive>(_ keys: [String]) throws -> T {
        let value = try extractNested(keys)
        
        if let value = value as? T {
            return value
        } else {
            throw JSONParsingError.wrongType(key: keys.last ?? "",
                                             type: value.dynamicType, expectedType: T.self)
        }
    }
    
    private func extractNested(_ keys: [String]) throws -> AnyObject? {
        let value: AnyObject? = try keys.reduce(json as AnyObject?) { js, key in
            if let js = js as? [String: AnyObject] {
                return js[key]
            } else {
                throw JSONParsingError.notFound(key: key)
            }
        }
        
        return value
    }

    
    // MARK: Helpers
    // Don't call this method from code. Use it only in debugger console
    public func debug() {
        let data = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        let string = String(data: data, encoding: .utf8)
        print(string ?? "")
    }
}
