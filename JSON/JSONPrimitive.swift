
public protocol _JSONPrimitive {}

extension String: _JSONPrimitive {}
extension Int: _JSONPrimitive {}
extension Double: _JSONPrimitive {}
extension Float: _JSONPrimitive {}
extension Bool: _JSONPrimitive {}
