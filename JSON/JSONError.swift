
public enum JSONParsingError: ErrorProtocol {
    case notFound(key: String)
    case wrongType(key: String, type: Any.Type, expectedType: Any.Type)
    case notJsonConvertible
}
