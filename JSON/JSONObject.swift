
public protocol JSONObject {
    init(json: JSON) throws
}
