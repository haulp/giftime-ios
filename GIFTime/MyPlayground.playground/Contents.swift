//: Playground - noun: a place where people can play

import UIKit

let str = "2016-07-10 13:45:01"
let str2 = "1970-01-01 00:00:00"

let formatter = DateFormatter()
formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

let date = formatter.date(from: str)
let date2 = formatter.date(from: str2)

let date3 = Date(timeIntervalSince1970: 0)

date3 == date2