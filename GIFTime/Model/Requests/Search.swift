
import Network
import Foundation

public enum Rating: String {
    case familyFriendly = "pg", all = ""
}

public struct Search: GiphyRequest {
    public let keyword: String
    public let rating: Rating
    public let offset: Int?
    
    public let path = "gifs/search"
    public var parameters: [String : AnyObject] {
        return ["rating": rating.rawValue, "q": keyword, "offset": offset ?? 0]
    }
    
    public let parser = GiphyResponseParser.parse { try $0.map(GIF.init(json:)) }
}
