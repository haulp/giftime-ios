
import Network
import Foundation

public struct Trending: GiphyRequest {
    public let offset: Int?

    public let path = "gifs/trending"
    public var parameters: [String : AnyObject] {
        return ["offset": offset ?? 0]
    }
    public let parser = GiphyResponseParser.parse { try $0.map(GIF.init(json:)) }
}
