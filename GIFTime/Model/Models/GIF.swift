
import JSON
import Foundation

public struct GIF {
    public let id: String
    public let rating: Rating?
    public let size: CGSize
    public let url: URL
    public let isTrending: Bool
}

extension GIF: JSONObject {
    public init(json: JSON) throws {
        id = try json.get("id")
        rating = Rating(rawValue: try json.get("rating"))
        let widthInfo: String = try json.get(["images", "fixed_width_downsampled", "width"])
        let heightInfo: String = try json.get(["images", "fixed_width_downsampled", "height"])
        let urlString: String = try json.get(["images", "fixed_width_downsampled", "url"])
        let trendingDateString: String = try json.get("trending_datetime")
        
        isTrending = trendingDateString != "1970-01-01 00:00:00"
        
        guard let
            url = URL(string: urlString),
            width = Int(widthInfo),
            height = Int(heightInfo)
        else {
            throw JSONParsingError.notFound(key: "")
        }
        
        self.url = url
        self.size = CGSize(width: width, height: height)
    }
}
