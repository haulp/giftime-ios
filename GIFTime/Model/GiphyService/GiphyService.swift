
import Network
import Foundation

public class GiphyService: Service {
    public let baseURL = URL(string: "http://api.giphy.com/v1")!
    public let headers = ["content-type": "application/json"]
}
