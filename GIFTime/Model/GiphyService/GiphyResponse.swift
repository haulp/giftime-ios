
import JSON
import Network
import RxSwift

public struct GiphyResponse<Data> {
    public let result: Data
    public let pagination: Pagination?
}

public struct GiphyResponseParser {
    public static func parse<T>(dataParser: (JSON) throws -> T) -> (Data?) throws -> GiphyResponse<T> {
        return { data in
            guard let data = data else {
                throw NetworkError.noData
            }
            
            let json = try JSON.decode(data: data)
            let dataJSON: JSON = try json.get("data")
            
            return GiphyResponse(result: try dataParser(dataJSON),
                                 pagination: try json.get("pagination"))
        }
    }
    
    public static func parse<T>(dataParser: ([JSON]) throws -> T) -> (Data?) throws -> GiphyResponse<T> {
        return { data in
            guard let data = data else {
                throw NetworkError.noData
            }
            
            let json = try JSON.decode(data: data)
            let dataJSON: [JSON] = try json.get("data")
            
            return GiphyResponse(result: try dataParser(dataJSON),
                                 pagination: try json.get("pagination"))
        }
    }
}

public struct Pagination {
    public let totalCount: Int?
    public let count: Int
    public let offset: Int
    
    public func next() -> Pagination {
        return Pagination(totalCount: totalCount,
                          count: count, offset: offset + count)
    }
    
    public static func first() -> Pagination {
        return Pagination(totalCount: nil,
                          count: 25, offset: 0)
    }
}

extension Pagination: JSONObject {
    public init(json: JSON) throws {
        totalCount = json.get("total_count")
        count = try json.get("count")
        offset = try json.get("offset")
    }
}
