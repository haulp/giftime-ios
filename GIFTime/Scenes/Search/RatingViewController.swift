
import UIKit
import RxSwift
import RxCocoa

extension Rating {
    public var title: String {
        switch self {
        case .all: return "All"
        case .familyFriendly: return "Family friendly"
        }
    }
}

public final class RatingViewController: UITableViewController {
    
    public var selected: Rating = .all
    public let selectedRating: Driver<Rating>
    
    private let _selectedRating = PublishSubject<Rating>()
    private let ratings: [Rating] = [.all, .familyFriendly]
    
    override init(style: UITableViewStyle) {
        selectedRating = _selectedRating.asDriver(onErrorJustReturn: .all)
        super.init(style: style)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        title = "Select Rating"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ratings.count
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = ratings[indexPath.row].title
        
        if selected == ratings[indexPath.row] {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _selectedRating.onNext(ratings[indexPath.row])
    }
}
