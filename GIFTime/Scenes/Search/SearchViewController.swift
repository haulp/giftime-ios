
import UIKit
import RxSwift
import RxCocoa
import Kingfisher
import Whisper

public final class SearchViewController: UIViewController {
    
    private lazy var collectionView: UICollectionView = {
        let layout = WaterfallLayout()
        layout.delegate = self
        
        return {
            $0.register(GIFCollectionViewCell.self, forCellWithReuseIdentifier: "GIFCollectionViewCell")
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.delegate = self
            $0.dataSource = self
            return $0
            }(UICollectionView(frame: .zero, collectionViewLayout: layout))
    }()
    
    private lazy var ratingButton: UIBarButtonItem = {
        let button = UIBarButtonItem(title: "All", style: .plain, target: nil, action: nil)
        return button
    }()
    
    private lazy var titleView: FBShimmeringView = {
        let titleLabel = UILabel()
        titleLabel.text = self.viewModel.searchText
        titleLabel.textColor = #colorLiteral(red: 1, green: 0.99997437, blue: 0.9999912977, alpha: 1)
        titleLabel.sizeToFit()
        
        let shimmeringView = FBShimmeringView(frame: titleLabel.frame)
        shimmeringView.shimmeringSpeed = 150
        shimmeringView.shimmeringAnimationOpacity = 0.1
        shimmeringView.contentView = titleLabel
        
        return shimmeringView
    }()
    
    private let viewModel: SearchViewModel
    private let bag = DisposeBag()
    
    private var gifs: [GIFViewModel] = [] {
        didSet {
            if oldValue.count != gifs.count {
                collectionView.reloadData()
            }
        }
    }
    
    public init(viewModel: SearchViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func loadView() {
        automaticallyAdjustsScrollViewInsets = false
        view = UIView()
        view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        view.addSubview(collectionView)
        [
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.bottomLayoutGuide.topAnchor)
            ].forEach { $0.isActive = true }
        
        navigationItem.titleView = titleView
        
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        navigationItem.rightBarButtonItem = ratingButton
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }
    
    public override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .lightContent
    }
    
    private func bindViewModel() {
        ratingButton.rx_tap.subscribeNext { [unowned self] in
            self.presentRatingSelect()
        }.addDisposableTo(bag)
        
        viewModel.ratingTitle.driveNext { [unowned self] title in
            self.ratingButton.title = title
        }.addDisposableTo(bag)
        
        viewModel.gifs.driveNext { [unowned self] gifs in
            self.gifs = gifs
        }.addDisposableTo(bag)
        
        rx_sentMessage(#selector(UICollectionViewDelegate.collectionView(_:willDisplay:forItemAt:)))
            .map { $0[2] as! NSIndexPath }
            .filter { [unowned self] indexPath in indexPath.item == self.gifs.count - 1 }
            .map { _ in () }
            .bindTo(viewModel.next)
            .addDisposableTo(bag)
        
        viewModel.isLoading.drive(titleView.rx_shimmering)
            .addDisposableTo(bag)
        
        viewModel.error.driveNext { [unowned self] message in
            self.showError(message: message)
        }.addDisposableTo(bag)

        
        viewModel.next.onNext()
    }
    
    private func presentRatingSelect() {
        let ratingViewController = RatingViewController(style: .plain)
        ratingViewController.selected = viewModel.rating.value
        
        ratingViewController.selectedRating.driveNext { [unowned self] rating in
            self.viewModel.rating.value = rating
            self.dismiss(animated: true, completion: nil)
        }.addDisposableTo(bag)
        
        ratingViewController.preferredContentSize = CGSize(width: 200, height: 100)
        let navigationController = UINavigationController(rootViewController: ratingViewController)
        navigationController.modalPresentationStyle = .popover
        let popover = navigationController.popoverPresentationController
        popover?.barButtonItem = ratingButton
        popover?.delegate = self
        
        present(navigationController, animated: true, completion: nil)
    }
    
    private func showError(message: String?) {
        if let message = message {
            let message = Message(title: message, backgroundColor: #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1))
            whisper(message: message, to: navigationController!, action: .Present)
        } else {
            silent(controller: navigationController!)
        }
    }
}

extension SearchViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gifs.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GIFCollectionViewCell", for: indexPath) as? GIFCollectionViewCell else {
            fatalError("cell must be GIFCollectionViewCell")
        }
        
        let gifViewModel = gifs[indexPath.item]
        
        cell.backgroundColor = gifViewModel.backgroundColor
        cell.imageView.animationImages = nil
        cell.trendImageView.isHidden = !gifViewModel.isTrending
        _ = cell.imageView.kf_setImageWithURL(gifViewModel.url)
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == gifs.count - 1 {
            print("^^^ Display cell cell cell")
        }
    }
}

extension SearchViewController: WaterfallLayoutDelegate {
    public func itemHeight(at indexPath: IndexPath) -> CGFloat {
        let gif = gifs[indexPath.item]
        let width = (view.frame.width - 36) / 2
        return gif.heightFor(width: width)
    }
}

extension SearchViewController: UIPopoverPresentationControllerDelegate {
    public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
