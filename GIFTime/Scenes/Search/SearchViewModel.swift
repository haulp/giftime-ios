
import RxSwift
import RxCocoa

public final class SearchViewModel {
    // Input
    public let next = PublishSubject<Void>()
    public let rating = Variable<Rating>(.all)
    public let searchText: String
    
    // Outputs
    public var ratingTitle: Driver<String> = Driver.never()
    public var isLoading: Driver<Bool> = Driver.never()
    public var gifs: Driver<[GIFViewModel]> = Driver.never()
    public var error: Driver<String?> = Driver.never()
    
    // Private
    private let _error = PublishSubject<String?>()
    private let _gifs = Variable<[GIFViewModel]>([])
    private let currentPage = Variable(Pagination.first())
    private let service: GiphyService
    private let bag = DisposeBag()
    
    public init(searchText: String, apiService: GiphyService) {
        self.service = apiService
        self.searchText = searchText
        
        let loading = ActivityIndicator()
        isLoading = loading.asDriver()
        
        ratingTitle = rating.asDriver().map { $0.title }
        gifs = _gifs.asDriver()
        
        let ratingChanged = rating.asDriver()
            .doOnNext { [unowned self] _ in self._gifs.value = [] }
        
        Driver.combineLatest(ratingChanged, next.asDriver(onErrorJustReturn: ())) { rating, _ in rating }
            .doOnNext { [unowned self] _ in
                print("^^ Next")
                self._error.onNext(nil)
            }
            .flatMapLatest { [unowned self] rating -> Driver<[GIF]> in
                let search = Search(keyword: self.searchText, rating: rating, offset: self.currentPage.value.next().offset)
                return apiService.load(request: search)
                    .trackActivity(loading)
                    .doOnError(onError: { [weak self] error in
                        guard let strongSelf = self else { return }
                        let message = strongSelf.getMessageFrom(error: error)
                        strongSelf._error.onNext(message)
                    })
                    .doOnNext(onNext: { [weak self] response in
                        self?.currentPage.value = response.pagination?.next() ?? Pagination.first()
                    })
                    .map { $0.result }
                    .asDriver(onErrorJustReturn: [])
            }
            .map { $0.map(GIFViewModel.init(gif:)) }
            .driveNext { [weak self] gifs in
                guard let strongSelf = self else { return }
                strongSelf._gifs.value += gifs
            }.addDisposableTo(bag)
    }
    
    private func getMessageFrom(error: ErrorProtocol) -> String {
        return "An Error Occured"
    }
}
