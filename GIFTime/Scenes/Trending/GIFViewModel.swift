
import RxSwift
import RxCocoa
import Foundation
import UIKit

public final class GIFViewModel {
    
    private let gif: GIF
    
    public var url: URL {
        return gif.url
    }
    
    public var aspectRatio: CGFloat {
        if gif.size.width == gif.size.height { return 1 }   // For zero width height
        return gif.size.width / gif.size.height
    }
    
    public lazy var backgroundColor: UIColor = {
        return self.getRandomColor()
    }()
    
    public var isTrending: Bool {
        return gif.isTrending
    }
    
    public init(gif: GIF) {
        self.gif = gif
    }
    
    public func heightFor(width: CGFloat) -> CGFloat {
        return width / aspectRatio
    }
    
    private func getRandomColor() -> UIColor {
        let colors: [UIColor] = [#colorLiteral(red: 0.3586547077, green: 0, blue: 0.1448745728, alpha: 1), #colorLiteral(red: 0.4776530862, green: 0.2292086482, blue: 0.9591622353, alpha: 1), #colorLiteral(red: 0.4028071761, green: 0.7315050364, blue: 0.2071235478, alpha: 1), #colorLiteral(red: 0.9385069609, green: 0.5891591311, blue: 0.4726046324, alpha: 1)]
        let random = arc4random_uniform(4)
        
        return colors[Int(random)]
    }
}
