
import UIKit
import RxSwift
import RxCocoa
import Kingfisher
import Whisper

public final class TrendingViewController: UIViewController {
    
    private lazy var collectionView: UICollectionView = {
        let layout = WaterfallLayout()
        layout.delegate = self
        
        return {
            $0.register(GIFCollectionViewCell.self, forCellWithReuseIdentifier: "GIFCollectionViewCell")
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.delegate = self
            $0.dataSource = self
            $0.keyboardDismissMode = .onDrag
            return $0
        }(UICollectionView(frame: .zero, collectionViewLayout: layout))
    }()
    
    private lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.barStyle = .black
        searchBar.keyboardAppearance = .dark
        return searchBar
    }()
    
    private lazy var titleView: FBShimmeringView = {
        let titleImageView = UIImageView(image: #imageLiteral(resourceName: "headerLogo"))
        titleImageView.sizeToFit()
        
        let shimmeringView = FBShimmeringView(frame: titleImageView.frame)
        shimmeringView.shimmeringSpeed = 150
        shimmeringView.shimmeringAnimationOpacity = 0.1
        shimmeringView.contentView = titleImageView
        
        return shimmeringView
    }()
    
    private let viewModel: TrendingViewModel
    private let bag = DisposeBag()
    
    private var gifs: [GIFViewModel] = [] {
        didSet {
            if oldValue.count != gifs.count {
                collectionView.reloadData()
            }
        }
    }
    
    public init(viewModel: TrendingViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func loadView() {
        automaticallyAdjustsScrollViewInsets = false
        view = UIView()
        view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        view.addSubview(collectionView)
        view.addSubview(searchBar)
        
        [
            searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            searchBar.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.bottomLayoutGuide.topAnchor)
        ].forEach { $0.isActive = true }
        
        navigationItem.titleView = titleView
        
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: #colorLiteral(red: 1, green: 0.99997437, blue: 0.9999912977, alpha: 1)]
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }
    
    public override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .lightContent
    }
    
    private func bindViewModel() {
        viewModel.gifs.driveNext { [weak self] gifs in self?.gifs = gifs }
            .addDisposableTo(bag)
        
        rx_sentMessage(#selector(UICollectionViewDelegate.collectionView(_:willDisplay:forItemAt:)))
            .map { $0[2] as! NSIndexPath }
            .filter { [unowned self] indexPath in indexPath.item == self.gifs.count - 1 }
            .map { _ in () }
            .bindTo(viewModel.next)
            .addDisposableTo(bag)
        
        viewModel.isLoading.drive(titleView.rx_shimmering)
            .addDisposableTo(bag)
        
        viewModel.error.driveNext { [unowned self] message in
            self.showError(message: message)
        }.addDisposableTo(bag)
        
        searchBar.rx_searchButtonClicked.subscribeNext { [unowned self] in
            self.searchBar.resignFirstResponder()
            let searchText = self.searchBar.text ?? ""
            let viewModel = self.viewModel.searchViewModel(searchText: searchText)
            let searchController = SearchViewController(viewModel: viewModel)
            self.navigationController?.pushViewController(searchController, animated: true)
        }.addDisposableTo(bag)
        
        viewModel.next.onNext()
    }
    
    private func showError(message: String?) {
        if let message = message {
            let message = Message(title: message, backgroundColor: #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1))
            whisper(message: message, to: navigationController!, action: .Present)
        } else {
            silent(controller: navigationController!)
        }
    }
}

extension TrendingViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gifs.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GIFCollectionViewCell", for: indexPath) as? GIFCollectionViewCell else {
            fatalError("cell must be GIFCollectionViewCell")
        }
        
        let gifViewModel = gifs[indexPath.item]
        
        cell.backgroundColor = gifViewModel.backgroundColor
        cell.imageView.animationImages = nil
        cell.trendImageView.isHidden = !gifViewModel.isTrending
        _ = cell.imageView.kf_setImageWithURL(gifViewModel.url)
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == gifs.count - 1 {
            print("^^^ Display cell cell cell")
        }
    }
}

extension TrendingViewController: WaterfallLayoutDelegate {
    public func itemHeight(at indexPath: IndexPath) -> CGFloat {
        let gif = gifs[indexPath.item]
        let width = (view.frame.width - 36) / 2
        return gif.heightFor(width: width)
    }
}
