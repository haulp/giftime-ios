
import UIKit
import Kingfisher

public final class GIFCollectionViewCell: UICollectionViewCell {
    public private(set) var imageView: AnimatedImageView
    public private(set) var trendImageView: UIImageView
    
    override init(frame: CGRect) {
        imageView = AnimatedImageView()
        trendImageView = UIImageView(image: #imageLiteral(resourceName: "trend"))
        
        super.init(frame: .zero)
        
        self.backgroundColor = .red()
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        trendImageView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(imageView)
        self.addSubview(trendImageView)
        [
            imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            imageView.topAnchor.constraint(equalTo: self.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            trendImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12),
            trendImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12)
        ].forEach { $0.isActive = true }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
