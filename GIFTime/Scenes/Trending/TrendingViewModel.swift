
import RxSwift
import RxCocoa

public final class TrendingViewModel {
    
    // Input
    public let next = PublishSubject<Void>()
    
    // Outputs
    public var isLoading: Driver<Bool> = Driver.never()
    public var gifs: Driver<[GIFViewModel]> = Driver.never()
    public var error: Driver<String?> = Driver.never()
    
    // Private
    private let _error = PublishSubject<String?>()
    private let currentPage = Variable(Pagination.first())
    private let service: GiphyService
    
    public init(apiService: GiphyService) {
        self.service = apiService
        
        let loading = ActivityIndicator()
        isLoading = loading.asDriver()
        
        error = _error.asDriver(onErrorJustReturn: "")
        
        gifs = next.asDriver(onErrorJustReturn: ())
            .doOnNext { [unowned self] in
                self._error.onNext(nil)
            }
            .withLatestFrom(currentPage.asDriver())
            .flatMapLatest { page in
                return apiService.load(request: Trending(offset: page.offset))
                    .trackActivity(loading)
                    .doOnError(onError: { [weak self] error in
                        guard let strongSelf = self else { return }
                        let message = strongSelf.getMessageFrom(error: error)
                        strongSelf._error.onNext(message)
                    })
                    .doOnNext(onNext: { [weak self] response in
                        self?.currentPage.value = response.pagination?.next() ?? Pagination.first()
                    })
                    .map { $0.result }
                    .asDriver(onErrorJustReturn: [])
            }
            .map { $0.map(GIFViewModel.init(gif:)) }
            .scan([GIFViewModel](), accumulator: { pre, curr in return pre + curr })
    }
    
    public func searchViewModel(searchText: String) -> SearchViewModel {
        let searchViewModel = SearchViewModel(searchText: searchText, apiService: service)
        
        return searchViewModel
    }
    
    private func getMessageFrom(error: ErrorProtocol) -> String {
        return "An Error Occured"
    }
}
