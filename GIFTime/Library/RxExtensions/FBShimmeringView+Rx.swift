
import RxSwift
import RxCocoa

extension FBShimmeringView {
    public var rx_shimmering: AnyObserver<Bool> {
        return UIBindingObserver(UIElement: self) { shimmeringView, shimmering in
            shimmeringView.isShimmering = shimmering
        }.asObserver()
    }
}
