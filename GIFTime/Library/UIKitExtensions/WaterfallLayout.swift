
import UIKit

public protocol WaterfallLayoutDelegate {
    func itemHeight(at indexPath: IndexPath) -> CGFloat
}

public final class WaterfallLayout: UICollectionViewLayout {
    
    public var delegate: WaterfallLayoutDelegate?
    private let columnCount = 2
    private let cellPadding: CGFloat = 6
    private var cache = [UICollectionViewLayoutAttributes]()
    
    private var contentHeight: CGFloat = 0
    private var contentWidth: CGFloat {
        let insets = collectionView?.contentInset ?? .zero
        return collectionView?.bounds.width ?? 0 - (insets.left + insets.right)
    }
    
    public override func prepare() {
        
        var cache = [UICollectionViewLayoutAttributes]()
        
        let columnWidth = contentWidth / CGFloat(columnCount)
        var xOffset = [CGFloat]()
        for column in 0 ..< columnCount {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        var column = 0
        var yOffset = Array<CGFloat>(repeating: 0, count: columnCount)
        
        for item in 0 ..< collectionView!.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: item, section: 0)
            
            let itemHeight = delegate?.itemHeight(at: indexPath) ?? 0
            let height = 2 * cellPadding + itemHeight
            let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height)
            let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
            
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            cache.append(attributes)
            
            contentHeight = max(contentHeight, frame.maxY)
            yOffset[column] = yOffset[column] + height
            
            column = column >= (columnCount - 1) ? 0 : column + 1
        }
        
        self.cache = cache
    }
    
    public override func collectionViewContentSize() -> CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    public override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                layoutAttributes.append(attributes)
            }
        }
        return layoutAttributes
    }
}
