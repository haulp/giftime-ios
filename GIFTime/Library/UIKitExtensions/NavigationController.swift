
import UIKit

public final class NavigationController: UINavigationController {
    public override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle() ?? .default
    }
}
