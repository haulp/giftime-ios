
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        let apiService = GiphyService()
        let trendingViewModel = TrendingViewModel(apiService: apiService)
        let trendingViewController = TrendingViewController(viewModel: trendingViewModel)
        let navigationController = NavigationController(rootViewController: trendingViewController)
        
        let window = UIWindow(frame: UIScreen.main().bounds)
        window.rootViewController = navigationController
        
        self.window = window
        window.makeKeyAndVisible()
                
        return true
    }
}
