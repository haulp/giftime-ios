# GIFTime #

Sample application that uses mvvm pattern and FRP (RxSwift). Written in swift 3.

### Requeirements ###

* xcode 8 beta 2
* iOS 10
* swift 3
* carthage

### How to run project ###

Clone the repo then just run `carthage bootstrap --platform iOS`. After the carthage downloads and compiles dependencies open `GIFTime.xcodeproj`.