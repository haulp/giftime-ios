
import RxSwift
import RxCocoa
import Foundation

public protocol Service {
    var session: URLSession { get }
    var headers: [String: String] { get }
    var baseURL: URL { get }
    
    func load<T: Request>(request: T) -> Observable<T.Result>
    func fetchRequest(urlRequest: URLRequest) -> Observable<Data?>
    func createURLRequest<T: Request>(request: T) -> Observable<URLRequest>
}

extension Service {
    
    public var headers: [String: String] { return [:] }
    public var session: URLSession { return URLSession.shared }
    
    public func load<T: Request>(request: T) -> Observable<T.Result> {
        return createURLRequest(request: request)
            .flatMap(fetchRequest(urlRequest:))
            .map(request.parser)
            .observeOn(MainScheduler.instance)
    }
    
    public func createURLRequest<T: Request>(request: T) -> Observable<URLRequest> {
        return Observable.create { observer in
            do {
                observer.onNext(try request.convertToURLRequest(baseURL: self.baseURL,
                                                            additionalHeaders: self.headers))
                observer.onCompleted()
            } catch {
                observer.onError(error)
            }
            return NopDisposable.instance
        }
    }
    
    public func fetchRequest(urlRequest: URLRequest) -> Observable<Data?> {
        return Observable.create { observer in
    
            let task = self.session.dataTask(with: urlRequest) { data, response, error in
                if let error = error {
                    print("^^ ERROR Network \(error)")
                    if (error.code == -1004 || error.code == -1009) && error.domain == NSURLErrorDomain {
                        observer.onError(NetworkError.noConnection)
                    } else {
                        observer.onError(NetworkError.other(error))
                    }
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else {
                    observer.onError(NetworkError.noResponse)
                    return
                }
                
                switch httpResponse.statusCode {
                case 200..<300:
                    observer.onNext(data)
                    observer.onCompleted()
                case _:
                    observer.onError(NetworkError.badStatus(code: httpResponse.statusCode))
                }
            }
            
            task.resume()
            
            return AnonymousDisposable {
                task.cancel()
            }
        }
    }
}
