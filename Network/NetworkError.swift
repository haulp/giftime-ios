
import Foundation
import JSON

public enum NetworkError: ErrorProtocol {
    case noConnection
    case noResponse
    case invalidURL
    case invalidParameters
    case other(NSError?)
    case noData
    case badStatus(code: Int)
}
