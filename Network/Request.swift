
import RxSwift
import Foundation

public protocol Request: URLRequestConvertible {
    associatedtype Result
    
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var defaultParameters: [String: AnyObject] { get }
    var parameters: [String: AnyObject] { get }
    var headers: [String: String] { get }
    var parser: (Data?) throws -> Result { get }
}

extension Request {
    public var defaultParameters: [String: AnyObject] {
        return [:]
    }
    
    public var parameters: [String: AnyObject] {
        return [:]
    }
    
    public var httpMethod: HTTPMethod {
        return .GET
    }
    
    public var headers: [String: String] {
        return [:]
    }
    
    public var parser: (Data?) throws -> Void {
        return { _ in  }
    }
}
