
func + <K, V> (left: [K:V], right: [K:V]) -> [K: V] {
    var dict = [K:V]()
    for (k, v) in right {
        dict.updateValue(v, forKey: k)
    }
    
    for (k, v) in left {
        dict.updateValue(v, forKey: k)
    }
    
    return dict
}
