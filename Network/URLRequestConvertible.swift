
import Foundation

public protocol URLRequestConvertible {
    func convertToURLRequest(baseURL: URL, additionalHeaders: [String: String]) throws -> URLRequest
}

extension URLRequestConvertible where Self: Request {
    
    public func convertToURLRequest(baseURL: URL, additionalHeaders: [String: String] = [:]) throws -> URLRequest {
        guard let url: URL = try? baseURL.appendingPathComponent(path) else {
            throw NetworkError.invalidURL
        }
        
        let allParameters = parameters + defaultParameters
        
        var urlRequest: URLRequest
        
        switch httpMethod {
        case .GET:
            var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
            components?.queryItems = allParameters.map { URLQueryItem(name: $0.0, value: "\($0.1)") }
            guard let url = components?.url else {
                throw NetworkError.invalidURL
            }
            urlRequest = URLRequest(url: url)
        case .POST:
            urlRequest = URLRequest(url: url)
            guard let body = try? JSONSerialization.data(withJSONObject: allParameters, options: []) else {
                throw NetworkError.invalidParameters
            }
            urlRequest.httpBody = body
        }
        urlRequest.httpMethod = httpMethod.rawValue
        additionalHeaders.forEach { urlRequest.setValue($0.1, forHTTPHeaderField: $0.0) }
        headers.forEach { urlRequest.setValue($0.1, forHTTPHeaderField: $0.0) }
        
        return urlRequest
    }
}
